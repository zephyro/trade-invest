


$( document ).ready(function() {

    $(".btn_modal").fancybox({
        'padding'    : 0
    });



    $('.btn_scroll').click(function(){
        var str=$(this).attr('href');
        $.scrollTo(str, 500);
        return false;
    });


    var commandUser = 0,
        commandProfit = 0;

    if($.cookie('cookie_user')) {
        commandUser = parseInt($.cookie('cookie_user'));
    }

    else {
        commandUser = parseInt($('.command_user').text());
    }

    if($.cookie('cookie_profit')) {
        commandProfit = parseInt($.cookie('cookie_profit'));
    }

    else {
        commandProfit = parseInt($('.command_profit').text());
    }

    setInterval(function(){

        commandUser = commandUser + 1;
        $('.command_user').text(commandUser);

        $.cookie('cookie_user', commandUser, {
            expires: 7
        });

    }, 1000);

    setInterval(function(){

        commandProfit = commandProfit + 2;
        $('.command_profit').text(commandProfit);

        $.cookie('cookie_profit', commandProfit, {
            expires: 7
        });

    }, 1000);

    $('.btn_send').click(function() {

        $('body').find('form:not(this)').children('div').removeClass('red'); //удаление всех сообщение об ошибке(валидатора)
        var answer = checkForm($(this).closest('form').get(0)); //ответ от валидатора
        if(answer != false)
        {
            var $form = $(this).closest('form'),
                email = $('input[name="email"]', $form).val(),
                phone = $('input[name="phone"]', $form).val(),
                name  = $('input[name="name"]', $form).val();

            $.ajax({
                type: "POST",
                url: "form-handler.php",
                data: {email:email, phone:phone, name:name}
            }).done(function(msg) {
                $('form').find('input[type=text], textarea').val('');
                console.log('удачно');
                $('.btn_thanks').trigger('click');
                setTimeout('location.replace("https://grant-epos.com/")',5000);
            });
        }
    });


});
