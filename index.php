<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
        <link rel="stylesheet" href="js/vendor/jquery.fancybox/jquery.fancybox.min.css">

        <link rel="stylesheet" href="css/main.css">

    </head>
    <body>

        <div class="page">

	        <section class="one">
		        <div class="container">
			        <div class="one__logo">
				        <img src="img/logo.png" class="img-fluid" alt="">
			        </div>
			        <h1>Вы в поиске надежного и стабильного дохода?</h1>
			        <div class="one__text">
				        Не теряйте времени на изучение инвестиционного рынка!<br/>
				        Воспользуйтесь моей находкой, после долгого поиска стабильного и надежного дохода!
			        </div>
		        </div>
	        </section>

	        <section class="two">
		        <div class="container">
			        <div class="two__text">По статистике 90% людей сливают свои деньга на финансовых рынках по причине незнания!</div>
			        <div class="two__heading">Вы хотите быть в числе оставшихся 10%?</div>
		        </div>
		        <div class="two__bottom"></div>
	        </section>

	        <section class="three">
		        <div class="container">
			        <div class="three__block">
				        <div class="three__heading"><strong>Профессиональное обучение</strong> по заработку на финансовом рынке</div>
				        <div class="three__row">
					        <div class="three__left">
						        <div class="three__text">
							        <strong>Доход от 24% в месяц</strong>
							        после 5 минут изучения
							        информации на данной странице!
						        </div>
						        <ul class="three__skills">
							        <li>
								        <strong>Ксения Афимова</strong>
								        <span>Аналитик финансового рынка</span>
							        </li>
							        <li>
								        <span>Опыт работы: 7 лет</span>
								        <span>Инвестиции в обороте 101 072$</span>
							        </li>
						        </ul>
					        </div>
					        <div class="three__right">
						        <div class="three__box">
							        <ul>
								        <li>
									        <div>Моя команда</div>
									        <strong><span class="command_user">326</span> <sub>чел.</sub></strong>
								        </li>
								        <li>
									        <div>Доход команды</div>
									        <strong><span class="command_profit">7824</span>%</strong>
								        </li>
							        </ul>
							        <a href="#contact" class="btn btn_scroll">Присоединиться</a>
						        </div>
					        </div>
				        </div>
			        </div>
					<div class="three__woman">
						<img src="img/three_woman.png" class="img-fluid" alt="">
					</div>
		        </div>
		        <div class="three__bottom"></div>
	        </section>

	        <section class="four">
		        <div class="container">

			        <ul class="four__features">
				        <li>
					        <strong>Секретные</strong>
					        <span>методики заработка</span>
				        </li>
				        <li>
					        <strong>Получение</strong>
					        <span>прибыли через 24 часа</span>
				        </li>
				        <li>
					        <strong>10 минут</strong>
					        <span>вашего времени</span>
				        </li>
			        </ul>

			        <div class="four__heading">
				        <span>Для начала,</span>
				        <strong>давайте познакомимся!</strong>
			        </div>

			        <div class="four__text">
				        <p>
					        Инвестиции давно перестали быть прерогативой крупных держателей капитала - сегодня инвестором может стать каждый, независимо от размера кошелька, уровня интелекта или гендерных признаков. Это один из немногих способов, который может привести к пассивному доходу и компания Grant Epos станет верным соратником для каждого инвестора, готового выйти из зоны комфорта и усремиться на поиски новой жизни.
					        В наших руках сосредоточены различные инвестиционные инстуременты, которыми мы виртуозно владеем — мы готовы открыть вам путь
					        к возможности и успеху.
				        </p>
				        <p>
					        Grant Epos — международная инвестиционная компания со своей философией и трезвым взглядом в завтрашней день. Мы не берем на себя обязательств, которые не могли бы выполнить, и несем ответственность за каждый внесенный в систему депозит. Профессиональная команда, инновационный подход и перспективные направления заработка — вот наша формула успеха, которой мы готовы поделиться с вами!
				        </p>
			        </div>

			        <div class="four__woman">
				        <img src="img/four__woman.png" class="img-fluid" alt="">
			        </div>

		        </div>
	        </section>

	        <section class="five">
		        <div class="container">
			        <div class="five__heading">Вы нашли себя в моем рассказе?</div>
			        <div class="five__text">Если да, вам осталось сделать последние шаги к финансовой свободе!</div>
			        <div class="five__row">
				        <div class="five__left">
					        <ol class="five__step">
						        <li><span><span>Изучить принцип данной методики дочитав данную страницу до конца</span></span></li>
						        <li><span><span>Подтвердить свое желание указав email адрес на который будет выслана инструкция</span></span></li>
						        <li><span><span>Уделяй 10 минут времени получай от 24% в месяц</span></span></li>
					        </ol>
				        </div>
				        <div class="five__right">
					        <div class="five__info">
						        <div class="five__info_image">
							        <img src="img/five__image.png" class="img-fluid" alt="">
						        </div>
						        <span class="five__info_text">Реальный надежный доход</span>
					        </div>
				        </div>
			        </div>
		        </div>
		        <span class="five__chars_top"></span>
	        </section>

	        <section class="six">
		        <div class="container">
					<div class="six__heading">
						<strong>Как это работает?</strong>
						<span>С чего я получаю прибыль?</span>
					</div>
			        <div class="six__t1">Вы инвестируете средства</div>
			        <div class="six__t2">минимальная сумма 10$</div>
			        <div class="six__advantage">
				        <ul>
					        <li>
						        Вы являетесь инвестором
						        в доверительном управлении,
						        где вашими деньгами
						        управляют профессионалы!
					        </li>
					        <li>
						        Вам не нужно анализировать
						        ситуацию на рынке и по 24 часа
						        контролировать рынок!
					        </li>
					        <li>
						        За вас, всю работу выполнят
						        профессионалы вы только
						        получаете доход!
					        </li>
				        </ul>
			        </div>

			        <ul class="six__step">
				        <li class="six__step_item">
					        <strong>Инвестиционная компания</strong>
					        <span>Доверительное управление, специалисты которой торгуют на финансовом рынке</span>
				        </li>
				        <li class="six__step_item"><strong>Торговля на биржах криптовалюты</strong></li>
				        <li class="six__step_item"><strong>Инвестиции в доходные стартапы и ICO</strong></li>
				        <li class="six__step_item"><strong>Торговля на рынок форекс бинарные опционы</strong></li>
			        </ul>
			        <div class="six__bracket">
				        <img src="img/six__bracket.svg" class="img-fluid" alt="">
			        </div>
			        <div class="six__summary">Доход от 24% в месяц!</div>
			        <div class="six__slogan">
				        <p>Не думайте, над поиском лучшего предложения!</p>
				        <strong>Воспользуйтесь моим опытом</strong>
				        <span>на который я потратил 10 лет!</span>
			        </div>

			        <div class="six__form form" id="contact">
				        <form class="form" name="form" method="POST" action="javascript:void(0);">
					        <div class="six__form_title">Заполните поля ниже, подтвердите введеные данные и <strong>получите подробную схему заработка</strong></div>
					        <ul class="six__form_row">
						        <li>
							        <div class="form_group valid email required">
								        <input type="text" class="form_control" name="email" placeholder="Ваш email">
							        </div>
						        </li>
						        <li>
							        <div class="form_group">
								        <input type="text" class="form_control" name="phone" placeholder="Моб. телефон">
							        </div>
						        </li>
						        <li>
							        <div class="form_group">
								        <input type="text" class="form_control" name="name" placeholder="Ваше имя">
							        </div>
						        </li>
						        <li>
							        <button type="submit" class="btn btn_send">Подтвердить</button>
						        </li>
					        </ul>
				        </form>
			        </div>
		        </div>
	        </section>

	        <section class="seven">
		        <div class="container">
			        <div class="seven__heading">Ответы на часто задаваемые вопросы</div>
			        <ol class="faq">
				        <li>
					        <div class="faq__heading">Grant Epos — это официально зарегистрированная компания? </div>
					        <div class="faq__text">Да, мы имеем сертификат о регистрации в Великобритании, выданный в 2017 году. Его достоверность вы можете проверить по <a href="#">ссылке</a>.</div>
				        </li>
				        <li>
					        <div class="faq__heading">Компания имеет свой офис?</div>
					        <div class="faq__text">Официальное представителсьтво Grant Epos находится в Лондоне, каждый желающий может посетить наш офис, предстваительство договорились о встрече с представителями проекта.</div>
				        </li>
				        <li>
					        <div class="faq__heading">Чем занимается компания?</div>
					        <div class="faq__text">Grant Epos инвестирует средства сразу по нескольким перспективным направлениям: торговля на Forex и криптовалютном рынке, стартапы и ICO, бинарные опционы. Это позволяет эффективно диверсифицировать средства и обеспечить инвесторам проекта стабильные выплаты по депозитам.</div>
				        </li>
				        <li>
					        <div class="faq__heading">Несут ли инвестиции в Grant Epos какие-либо риски?</div>
					        <div class="faq__text">Любые инвестиции несут в себе риски, независимо от того, профессионалы управляют вашими средствами или вы сами. При сотрудничестве с Grant Epos стоит исключить неторговые риски, но оказать влияния на рыночную ситуацию, политическую ситуацию в мире и другие факторы, которые косвенно или прямо могут повлечь за собой потрею ваших средств, мы не в силах. Со своей стороны, мы обязуемся делать все, чтобы свести все возможные риски к минимуму, но это не исключает их полностью. Вы должны понимать это и вкладывать только те деньги, которые в случае потери не нанесут ущерб вашему благосостоянию.</div>
				        </li>
			        </ol>

			        <div class="seven__copy">©2018 Trade Invest. Все права защищены.</div>

		        </div>
	        </section>

        </div>

        <div class="hide">
	        <a href="#thanks" class="btn_thanks btn_modal"></a>
	        <div class="thanks" id="thanks">
		        <p>Инструкция отправлена на ваш <strong>е-маил адрес!</strong></p>
		        <p>Сейчас вы будете перенаправлены на <a href="https://grant-epos.com/">сайт компании.</p>
	        </div>
        </div>

        <script src="js/vendor/modernizr-3.5.0.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
        <script src="js/vendor/jquery.fancybox/jquery.fancybox.min.js"></script>
        <script src="js/vendor/jquery.cookie.js"></script>
        <script src="js/vendor/jquery.scrollTo.min.js"></script>
        <script src="js/vendor/validator.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>
